Source: arm-compute-library
Section: libs
Priority: optional
Maintainer: Compute Library Team <developer-compute@arm.com>
Uploaders: Georgios Pinitas <georgios.pinitas@arm.com>,
Standards-Version: 4.5.0
Homepage: https://github.com/ARM-software/ComputeLibrary
Vcs-git: https://salsa.debian.org/wookey/arm-compute-library
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 12),
               dh-exec (>=0.3),
               scons (>= 2.4),
               doxygen,
               graphviz

Package: libarm-compute20
Architecture: armhf arm64
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Arm Compute Library
 Arm Compute Library is a software library for computer vision and
 machine learning.  It is a collection of low-level functions
 optimized for Arm CPU and GPU architectures targeted at image
 processing, computer vision, and machine learning.  Arm Compute
 Library is usable as a shared or static library.
 .
 Both 32 and 64-bit variants are supported. (ARM v7, 32-bit, armhf,
 and ARM v8, 64-bit, aarch64/arm64)

Package: libarm-compute-dev
Section: libdevel
Architecture: armhf arm64
Multi-Arch: same
Depends: libarm-compute20 (= ${binary:Version}), ${misc:Depends}
Suggests: libarm-compute-doc (= ${source:Version})
Description: Arm Compute Library - development files
 Arm Compute Library is a software library for computer vision and
 machine learning.  It is a collection of low-level functions
 optimized for Arm CPU and GPU architectures targeted at image
 processing, computer vision, and machine learning.  Arm Compute
 Library is usable as a shared or static library.
 .
 Both 32 and 64-bit variants are supported: armhf (ARM v7, 32-bit),
 and arm64 (ARM v8, 64-bit, aarch64)
 .
 This package contains the development files (headers, static library)

Package: libarm-compute-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, libjs-mathjax
Multi-Arch: foreign
Description: Arm Compute Library - documentation
 Arm Compute Library is a software library for computer vision and
 machine learning.  It is a collection of low-level functions
 optimized for Arm CPU and GPU architectures targeted at image
 processing, computer vision, and machine learning.  Arm Compute
 Library is usable as a shared or static library.
 .
 Both 32 and 64-bit variants are supported: armhf (ARM v7, 32-bit),
 and arm64 (ARM v8, 64-bit, aarch64)
 .
 This package contains the Compute Library documentation as HTML.
